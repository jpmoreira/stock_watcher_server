/*jslint node:true*/
'use strict';

var models = require("../models");
// Locate your certificate
var join = require('path').join;
var pfx = join(__dirname, './_certs/SW_Cert.p12');

// Create a new agent
var apnagent = require('apnagent');
var agent = module.exports = new apnagent.Agent();

// set our credentials
agent.set('pfx file', pfx);

// our credentials were for development
agent.enable('sandbox');


agent.connect(function (err) {
  // gracefully handle auth problems
  if (err && err.name === 'GatewayAuthorizationError') {
    console.log('Authentication Error: %s', err.message);
    process.exit(1);
  }
  else if (err) { // handle any other err (not likely)
    throw err;
  }

  // it worked!
  var env = agent.enabled('sandbox') ? 'sandbox' : 'production';

  console.log('apnagent [%s] gateway connected', env);
});
/*
var device = require('./device');

agent.createMessage()
  .device(device)
  .alert('Hello Universe!')
  .send();

*/


// Job that will check every minute for stocks out of the desired bounds

