/* globals describe, process, it, since */

"use strict";

var supertest = require('supertest');
require('jasmine-custom-message');

var models = require("../models");
var dbFiller = require("../dbFiller.js");
var app_promise = require('../app.js');
var app;

process.on('uncaughtException', function (error) {
  console.log(error);
  console.log(error.stack);
});

describe('Login API', function () {


  it('should return an error if no login information is provided', function (done) {


    app_promise.then(function (theApp) {
      app = theApp;
      return models.sequelize.sync({ force: true });
    }).then(function () {
      return dbFiller.fill();
    }).then(function () {
      supertest(app)
        .get('/api/users/login')
        .expect(403)
        .end(function (err) {
          if (err) {
            since("Login Info is required : " + err).expect(false).toBe(true);
          }
          done();
        });
    });
  });

  it('should accept a correct login', function (done) {

    app_promise.then(function (theApp) {
      app = theApp;
      return models.sequelize.sync({ force: true });
    }).then(function () {
      return dbFiller.fill();
    }).then(function () {
      supertest(app)
        .get('/api/users/login')
        .auth("u1@mail.com", "1234")
        .expect(200)
        .end(function (err) {
          if (err) {
            since("users that have a login should be able to login: " + err).expect(false).toBe(true);
          }
          done();
        });
    });
  });


  it('should not accept an incorrect login', function (done) {
    app_promise.then(function (theApp) {
      app = theApp;
      return models.sequelize.sync({ force: true });
    }).then(function () {
      return dbFiller.fill();
    }).then(function () {
      supertest(app)
        .get('/api/users/login')
        .auth('u1@mail.com', '12345')
        .expect(403)
        .end(function (err) {
          if (err) {
            since("users that dont have a login shouldn't be able to login: " + err).expect(false).toBe(true);
          }
          done();
        });


    });
  });



});



describe('Register API', function () {

  it('Should require some data to be provided', function (done) {
    app_promise.then(function (theApp) {
      app = theApp;
      return models.sequelize.sync({ force: true });
    }).then(function () {
      return dbFiller.fill();
    }).then(function () {
      supertest(app)
        .put('/api/user/register')
        .expect(400)
        .end(function (err) {
          if (err) {
            since("users have to provide some information: " + err).expect(false).toBe(true);
          }
          done();
        });


    });
  });


  it('Should not allow a registered user to register itself again', function (done) {

    var userToRegister = {
      email: "u1@mail.com",
      password: "1234"
    };
    app_promise.then(function (theApp) {
      app = theApp;
      return models.sequelize.sync({ force: true });
    }).then(function () {
      return dbFiller.fill();
    }).then(function () {
      supertest(app)
        .put('/api/user/register')
        .type('application/json')
        .send(userToRegister)
        .expect(403)
        .end(function (err) {
          if (err) {
            since("registered users cannot register themselves again: " + err).expect(false).toBe(true);
          }
          done();
        });

    });
  });


  it('Should allow to register a new user', function (done) {

    var userToRegister = {
      email: "u10@mail.com",
      password: "1234"
    };
    app_promise.then(function (theApp) {
      app = theApp;
      return models.sequelize.sync({ force: true });
    }).then(function () {
      return dbFiller.fill();
    }).then(function () {
      supertest(app)
        .put('/api/user/register')
        .type('application/json')
        .send(userToRegister)
        .expect(200)
        .end(function (err) {
          if (err) {
            since("a new user should be able to register itself: " + err).expect(false).toBe(true);
          }
          done();
        });

    });
  });
});



describe('Devices API', function () {

  it('Should not allow to register a new device without auth', function (done) {

    app_promise.then(function (theApp) {
      app = theApp;
      return models.sequelize.sync({ force: true });
    }).then(function () {
      return dbFiller.fill();
    }).then(function () {
      supertest(app)
        .put('/api/user/submitID')
        .type('application/json')
        .send({ "deviceID": "1234" })
        .expect(403)
        .end(function (err) {
          if (err) {
            since("only an authenticated user should be able to add devices " + err).expect(false).toBe(true);
          }
          done();
        });

    });
  });

  it('Should not allow to register a new device with a fake auth', function (done) {

    app_promise.then(function (theApp) {
      app = theApp;
      return models.sequelize.sync({ force: true });
    }).then(function () {
      return dbFiller.fill();
    }).then(function () {
      supertest(app)
        .put('/api/user/submitID')
        .type('application/json')
        .auth("u1@mail.com", "12345")
        .send({ "deviceID": "1234" })
        .expect(403)
        .end(function (err) {
          if (err) {
            since("only an existing users should be able to add devices " + err).expect(false).toBe(true);
          }
          done();
        });

    });
  });


  it('Should allow a legit user to register a new device', function (done) {

    app_promise.then(function (theApp) {
      app = theApp;
      return models.sequelize.sync({ force: true });
    }).then(function () {
      return dbFiller.fill();
    }).then(function () {
      supertest(app)
        .put('/api/user/submitID')
        .type('application/json')
        .auth("u1@mail.com", "1234")
        .send({ "deviceID": "1234" })
        .expect(200)
        .end(function (err) {
          if (err) {
            since("existing users should be able to add new devices " + err).expect(false).toBe(true);
          }
          done();
        });

    });
  });
});



describe('Notifications API', function () {


  it('Should not allow an unauthenticated user to add notifications', (done) => {
    app_promise.then(function (theApp) {
      app = theApp;
      return models.sequelize.sync({ force: true });
    }).then(function () {
      return dbFiller.fill();
    }).then(function () {
      supertest(app)
        .put('/api/notification/addNotification')
        .type('application/json')
        .expect(400)
        .end(function (err) {
          if (err) {
            since("unauthenticated clients shoulnd't be able to register new authentication requests " + err).expect(false).toBe(true);
          }
          done();

        });

    });
  });



  it('Should not allow an unexisting user to add a notification', (done) => {

    var tick = { "max": 10, "min": 10, "tick": "AAPL", "stockQt": 10 };
    app_promise.then(function (theApp) {
      app = theApp;
      return models.sequelize.sync({ force: true });
    }).then(function () {
      return dbFiller.fill();
    }).then(function () {
      supertest(app)
        .put('/api/notification/addNotification')
        .type('application/json')
        .auth("u10@mail.com", "1234")
        .send(tick)
        .expect(403)
        .end(function (err) {
          if (err) {
            since("only clients should be able to register new authentication requests " + err).expect(false).toBe(true);
          }
          done();
        });

    });
  });

  it('Should not allow an existing user to add a notification if the required information is not provided', (done) => {
    app_promise.then(function (theApp) {
      app = theApp;
      return models.sequelize.sync({ force: true });
    }).then(function () {
      return dbFiller.fill();
    }).then(function () {
      supertest(app)
        .put('/api/notification/addNotification')
        .type('application/json')
        .auth("u1@mail.com", "1234")
        .expect(400)
        .end(function (err) {
          if (err) {
            since("the system requires the tick information to be provided " + err).expect(false).toBe(true);
          }
          done();
        });

    });
  });


  it('Should  allow an existing user to add a notification', (done) => {


    var tick = { "max": 10, "min": 10, "tick": "AAPL", "stockQt": 10 };

    app_promise.then(function (theApp) {
      app = theApp;
      return models.sequelize.sync({ force: true });
    }).then(function () {
      return dbFiller.fill();
    }).then(function () {
      supertest(app)
        .put('/api/notification/addNotification')
        .type('application/json')
        .send(tick)
        .auth("u1@mail.com", "1234")
        .expect(200)
        .end(function (err) {
          if (err) {
            since("a well formed request should be satisfied " + err).expect(false).toBe(true);
          }
          done();
        });

    });
  });


  it('Should not allow an unauthenticated user to remove a notification', (done) => {
    app_promise.then(function (theApp) {
      app = theApp;
      return models.sequelize.sync({ force: true });
    }).then(function () {
      return dbFiller.fill();
    }).then(function () {
      supertest(app)
        .delete('/api/notification/removeNotification')
        .type('application/json')
        .expect(400)
        .end(function (err) {
          if (err) {
            since("authentication is required to remove notifications " + err).expect(false).toBe(true);
          }
          done();
        });

    });
  });

  it('Should not allow an unexisting user to remove a notification', (done) => {
    app_promise.then(function (theApp) {
      app = theApp;
      return models.sequelize.sync({ force: true });
    }).then(function () {
      return dbFiller.fill();
    }).then(function () {
      supertest(app)
        .delete('/api/notification/removeNotification')
        .type('application/json')
        .auth("u100@mail.com", "1234")
        .send({"tickName":"AAPL","removeNotif":true})
        .expect(403)
        .end(function (err) {
          if (err) {
            since("authentication is required to remove notifications " + err).expect(false).toBe(true);
          }
          done();
        });

    });
  });


  it('Should allow an existing user to remove a notification', (done) => {
    app_promise.then(function (theApp) {
      app = theApp;
      return models.sequelize.sync({ force: true });
    }).then(function () {
      return dbFiller.fill();
    }).then(function () {
      supertest(app)
        .delete('/api/notification/removeNotification')
        .type('application/json')
        .auth("u1@mail.com","1234")
        .send({"tickName":"AAPL","removeNotif":true})
        .expect(200)
        .end(function (err) {
          if (err) {
            since("authentication is required to remove notifications " + err).expect(false).toBe(true);
          }
          done();
        });

    });
  });





});
  
  
 

  
