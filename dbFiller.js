"use strict";
var models = require("./models");

module.exports.fill = function () {

	var users = models.User.bulkCreate([
		{ email: 'u1@mail.com', password: "1234" },
		{ email: 'u2@mail.com', password: "1234" },
		{ email: 'u3@mail.com', password: "1234" }
    ]).then(function () { // Notice: There are no arguments here, as of right now you'll have to...
		return models.User.findAll();
    });

    var notifs = models.Notification.bulkCreate([
		{ tick: 'AAPL', max: 100.0, min: 50.0 },
		{ tick: 'GOOG', max: 1000.0, min: 0.0 },
		{ tick: 'AAPL', max: 1000.0, min: 0.0 },
		{ tick: 'GOOG', max: 100.0, min: 0.0 }
    ]).then(function () { return models.Notification.findAll(); });


    return Promise.all([users, notifs]).then(function (results) {

		var users = results[0];
		var notifs = results[1];

		notifs.forEach(function (notif, i) {

			var user = users[Math.floor(i / 2)];//two notifications for each user
			notif.setDataValue('UserId', user.id);
			notif.save();
			//user.addNotif(notif);
         
		});


    }).catch((error) => {
		
		console.error("Unable to fill db. Error: "+error);
		
	});

};