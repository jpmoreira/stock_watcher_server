"use strict";

var User = require("../models").User;
var Notification = require("../models").Notification;
var Device = require("../models").Device;

exports.userRegisterPut = function(args, res) {
  /**
   * parameters expected in the args:
   * user (User)
   **/

User.findOne({where:{email:args.user.value.email}}).then(function(user){
  
  if(user != null){
    throw "User already exists";
  }
  return User.create(
    {"email":args.user.value.email,
    "password":args.user.value.password
    });
  
}).then(function(){
  
  res.end("{}");
  
})
.catch(function(){
  
    res.statusCode = 403;
    res.end("{}");
  
});
};

exports.userSubmitIDPut = function(args, res) {
  /**
   * parameters expected in the args:
   * deviceID (String)
   **/
   

   
   User.findOne({where:{"email":args.user.email,"password":args.user.password}}).then(function(user){
     
     if(user === null){
       throw "No user found";
     }
     
   return Device.create({"deviceID":args.device.value.deviceID}).then(function(device){
       
       user.addDevice(device);
        res.end("{}");
     });
     
     
   })
   .catch(function(){
     
       res.statusCode = 403;
       res.end("{}");
     
   });
   

};
exports.usersLoginGet = function(args, res) {
  /**
   * parameters expected in the args:
   **/

   console.log("ARGS:\n");
   console.log(args);

  User.findOne({
    where: {
      "email": args.user.email,
      "password": args.user.password
    },
    include: [{
      attributes: ['tick', 'min', 'max', 'stockQt'],
      model: Notification, 
      as: 'notifs' 
      }]
      }).then(function(user) {
    
    if (user === null) {
      throw "User Not found";
    }
    
    if(user.notifs == null){user.notifs = [];}//empty array
    res.end(JSON.stringify(user));

  }).catch(function(err){
    
    
    res.statusCode = 403;
      res.end("{}");
      console.log(err);
      return;
    
  });
};
