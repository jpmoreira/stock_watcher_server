/*jslint node: true */
"use strict";
var Notification = require("../models").Notification;
var User = require("../models").User;

// ==================================================================

exports.notificationAddNotificationPut = function (args, res) {
  /**
   * parameters expected in the args:
   * tick (Notification)
   **/
   
   var user;

  User.findOne({ where: { "email": args.user.email, "password": args.user.password } }).then(function (theUser) {


   if( theUser == null){
     throw "User not found";
   }
   user = theUser;
  
   return Notification.findOrCreate(
      {
        where:
        {"tick": args.tick.value.tick,
        "UserId":user.id 
        },
        defaults:{"tick": args.tick.value.tick,
        max:args.tick.value.max,
        min:args.tick.value.min,
        stockQt:args.tick.value.stockQt
        }
        });

  }).then(function (resp) {

      var created = resp[1];
      var notif = resp[0];
      
      if(created){
        user.addNotif(notif);
      }
      else{   
        notif.set("max",args.tick.value.max);
        notif.set("min",args.tick.value.min);
        notif.set("stockQt",args.tick.value.stockQt);
        notif.save();
      }
      res.end("{}");

    }).catch(function(){
      
      res.statusCode = 403;
      res.end("{}");
      
      
    });

};

// ==================================================================

exports.notificationRemoveNotificationPut = function (args, res ) {

  /**
   * parameters expected in the args:
   * tick (String)
   **/

  User.findOne(
    { where: 
    { "email": args.user.email,
    "password": args.user.password 
    } 
    }).then(function (user) {
      
      if (user == null){
        throw "Inexisting user";
      }
    
    return user.getNotifs().filter(function(notif){
      return notif.tick == args.tick.value.tickName;
    });

  }).then(function(notifs){
      
      if(args.tick.value.removeNotif) {
        
        notifs[0].set("max", -1);
        notifs[0].set("min", -1);
        notifs[0].save();        
        
      } else {
        
        notifs[0].destroy();
          
      }
      
      res.end("{}");
      
    }).catch(()=>{
      
       res.statusCode = 403;
       res.end("{}");
      
    });
};
