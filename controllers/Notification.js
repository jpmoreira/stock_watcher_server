/*jslint node: true */
"use strict";

var auth = require("basic-auth");


var Notification = require('./NotificationService');


module.exports.notificationAddNotificationPut = function notificationAddNotificationPut(req, res, next) {

  var credentials = auth(req);

  if (credentials) {
    req.swagger.params.user = {
      email: credentials.name,
      password: credentials.pass
    };
  }
  else {
    res.statusCode = 403;
    res.end("{}");
    return;
  }
  
  /*
  req.swagger.params.user = {
      email: "jpmoreira@hotmail.com",
      password: "1234"
    }
    */
  Notification.notificationAddNotificationPut(req.swagger.params, res, next);
};

module.exports.notificationRemoveNotificationPut = function notificationRemoveNotificationPut(req, res, next) {


  var credentials = auth(req);

  if (credentials) {
    req.swagger.params.user = {
      email: credentials.name,
      password: credentials.pass
    };
  }
  else {
    res.statusCode = 403;
    res.end("{}");
    return;
  }
  
/*
  req.swagger.params.user = {
      email: "jpmoreira@hotmail.com",
      password: "1234"
    }
  */  
  Notification.notificationRemoveNotificationPut(req.swagger.params, res, next);
};
