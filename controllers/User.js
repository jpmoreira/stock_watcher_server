"use strict";

var User = require('./UserService');
var auth = require("basic-auth");


module.exports.userRegisterPut = function userRegisterPut(req, res, next) {
  User.userRegisterPut(req.swagger.params, res, next);
};

module.exports.userSubmitIDPut = function userSubmitIDPut(req, res, next) {
  var credentials = auth(req);
  
  if (credentials) {
    req.swagger.params.user = {
      email: credentials.name,
      password: credentials.pass
    };
  }
  else{
     res.statusCode = 403;
     res.end();
     return;
  }
  User.userSubmitIDPut(req.swagger.params, res, next);
};

module.exports.usersLoginGet = function usersLoginGet(req, res, next) {
  
  var credentials = auth(req);

  if (credentials) {
    req.swagger.params.user = {
      email: credentials.name,
      password: credentials.pass
    };
  } else {
     res.statusCode = 403;
     res.end("{}");
     return;
  }
  
  User.usersLoginGet(req.swagger.params, res, next);
  
};
