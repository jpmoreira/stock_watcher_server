/*
jslint node:true


*/
'use strict';

var app = require('connect')();

var swaggerTools = require('swagger-tools');
var jsyaml = require('js-yaml');
var fs = require('fs');

var models = require('./models');
var Promise = require("bluebird");

// swaggerRouter configuration
var options = {
  swaggerUi: '/swagger.json',
  controllers: './controllers',
  useStubs: process.env.NODE_ENV === 'development' ? true : false // Conditionally turn on stubs (mock mode)
};

// The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
var spec = fs.readFileSync('./api/swagger.yaml', 'utf8');
var swaggerDoc = jsyaml.safeLoad(spec);


if (process.env.NODE_ENV === 'development') {

  swaggerDoc.host = "localhost:" + (process.env.PORT || 8080);
}


module.exports = new Promise(function (resolve) {
  
  // Initialize the Swagger middleware
  swaggerTools.initializeMiddleware(swaggerDoc, function (middleware) {
    // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
    app.use(middleware.swaggerMetadata());

    // Validate Swagger requests
    app.use(middleware.swaggerValidator());

    // Route validated requests to appropriate controller
    app.use(middleware.swaggerRouter(options));

    // Serve the Swagger documents and Swagger UI
    app.use(middleware.swaggerUi());


    models.User.hasMany(models.Device, { as: "devices" });
    //models.Device.hasOne(models.User);
    //models.Notification.hasOne(models.User);
    models.User.hasMany(models.Notification, { as: "notifs" });

    resolve(app);

  });


});

