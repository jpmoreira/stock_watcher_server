"use strict";

var models = require("./models");
//var Promise = require("bluebird");
var http = require('http');
var yahoo_request = require("./yahoo_request.js");
var app_promise = require("./app.js");
var serverPort = process.env.PORT || 8080;
var agent = require("./notifications");
var dbFiller = require("./dbFiller.js");

var app;

app_promise.then(function (theApp) {
  app = theApp;
  return models.sequelize.sync({ force: true });
}).then(function () {
  return dbFiller.fill();
}).then(function () {
      
  //Launch api server running on the desired port
  http.createServer(app).listen(serverPort, function () {
    console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
    console.log('Swagger-ui is available on http://localhost:%d/docs', serverPort);

  });
  
      
  //Schedule notifications
  var cron = require('node-schedule');


  var rule = new cron.RecurrenceRule();
  rule.second = 30;
  cron.scheduleJob(rule, function () {



    models.Notification.findAll().map(function (notif) {
      return notif.tick;
    }).then(function (ticks) {

      yahoo_request(ticks, function (results) {


        models.User.findAll({
          include: [
            { model: models.Device, as: 'devices' },
            { model: models.Notification, as: 'notifs' }
          ]
        }).then(function (users) {

          notifyUsers(users, results);

        });
      });
    });

  });


});


var notifyUsers = function (users, results) {

  var outsideBounds = [];
  var insideBounds = [];

  // separate nots inside desired value from the ones outside
  users.forEach(function (u) {


    var outside = u.notifs.filter(function (n) {
      return results[n.tick] && (results[n.tick] > n.max || results[n.tick] < n.min);
    });

    outside.forEach(function (n) {// append devices info
      
      n.devices = u.devices.map(function (d) { return d.deviceID; });

      n.message = "" + n.tick + (results[n.tick] > n.max ? " felt to " : " raised to ") + results[n.tick];

    });

    var inside = u.notifs.filter(function (n) {
      return results[n.tick] && results[n.tick] <= n.max && results[n.tick] >= n.min;
    });

    //append
    outsideBounds.push.apply(outsideBounds, outside);
    insideBounds.push.apply(insideBounds, inside);

  });

  outsideBounds.forEach(function (n) {

    if (n.activeNotif) {

      notify(n.devices, n.message);

    }

    n.setDataValue("activeNotif", false);
    n.save();

  });

  insideBounds.forEach(function (n) {

    n.setDataValue("activeNotif", true);
    n.save();

  });

};

var notify = function (deviceIDs, message) {

  deviceIDs.forEach(function (deviceID) {
    agent.createMessage()
      .device(deviceID)
      .alert(message)
      .send();

  });

};
