/*jslint node:true*/
'use strict';
var request = require('request');


module.exports = function (ticks, onResult) {


  var parameters = {
    "f": "sl1cj1",
    "s": ticks.join(" ")
  };

  request.get({ url: "https://finance.yahoo.com/d/quotes", qs: parameters }, function (err, response, body) {
    if (err) { console.log(err); return; }

    var values = body.split("\n").map(function (line) {
      return line.split(",");
    }).filter(function (parts) {
      return parts.length > 2;
    })
      .reduce(function(prev,parts){
        prev[parts[0].replace(/\"/g, "")] = parts[1];
        return prev;
      },{});

    onResult(values);
  });


};

/*
module.exports(["AAPL", "GOOG","AAPL"], function (values) {


  console.log(values);

});
*/
