"use strict";

module.exports = function (sequelize, DataTypes) {

  var Device = sequelize.define("Device", {
    deviceID: {
      type: DataTypes.STRING,
      field: 'deviceID', // Will result in an attribute that is firstName when user facing but first_name in the database
      allowNull: false,
      unique: 'deviceIndex'
    }
  }, {
      freezeTableName: true // Model tableName will be the same as the model name
    });

  return Device;

};