"use strict";


module.exports = function (sequelize, DataTypes) {

  var Notification = sequelize.define("Notification", {
    tick: {
      type: DataTypes.STRING,
      field: 'tick', // Will result in an attribute that is firstName when user facing but first_name in the database
      allowNull: false
    },
    max: {
      type: DataTypes.FLOAT,
      field: "max",
      allowNull: false,
      set: function(val) {
        this.setDataValue('max', val);
      }
    },
    min: {
      type: DataTypes.FLOAT,
      field: "min",
      allowNull: false,
      set: function(val) {
        this.setDataValue('min', val);
      }
    },
    stockQt: {
       type: DataTypes.INTEGER,
       field: "stockQt",
       allowNull: false,
       defaultValue: 0
     },
     activeNotif: {
       type: DataTypes.BOOLEAN,
       field: "activeNotif",
       allowNull: true,
       defaultValue: true
     }
  }, {
      freezeTableName: true // Model tableName will be the same as the model name
    });

  return Notification;

};