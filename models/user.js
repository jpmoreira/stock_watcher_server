/*jslint node:true*/
"use strict";

module.exports = function (sequelize, DataTypes) {

  var User = sequelize.define("User", {
    id: {
      type: DataTypes.INTEGER,
      field: 'id',
      allowNull: false,
      unique: 'user_id',
      primaryKey: true,
      autoIncrement: true
    },
    email: {
      type: DataTypes.STRING,
      field: 'email', // Will result in an attribute that is firstName when user facing but first_name in the database
      allowNull: false,
      unique: 'emailIndex'
    },
    password: {
      type: DataTypes.STRING,
      field: "password",
      allowNull: false
    }
  }, {
      freezeTableName: true // Model tableName will be the same as the model name
    });

  return User;

};